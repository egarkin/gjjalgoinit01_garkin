import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.*;

public class Run {
    public static final int N_THREADS = 2;

    public static void main(String[] args) {
        String from, to, output;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Enter from currency:");
            from = scanner.next().toUpperCase().trim();
        } while (!InputChecker.check(from));
        do {
            System.out.println("Enter to currency:");
            to = scanner.next().toUpperCase().trim();
        } while (!InputChecker.check(to));
        String request = "http://api.fixer.io/latest?base=" + from + "&symbols=" + to;
        Date dateNow = new Date();
        SimpleDateFormat formatForDate = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = formatForDate.format(dateNow);
        String filename = currentDate + "\\" + from + "-" + to + ".txt";
        ExecutorService service = Executors.newFixedThreadPool(N_THREADS);
        Future<String> task = service.submit(new GetRate(request, filename));
        try {
            while (!task.isDone()) {
            System.out.print(".");
            TimeUnit.MILLISECONDS.sleep(5);
        }
            output = from + " => " + to + " : " + task.get();
            System.out.println(System.lineSeparator() + output);
        } catch (InterruptedException | ExecutionException ie) {
            System.out.println("Execution error: " + ie);
        }
        service.shutdown();
    }
}