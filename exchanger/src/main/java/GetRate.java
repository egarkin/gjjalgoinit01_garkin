import java.io.IOException;
import java.util.concurrent.Callable;

public class GetRate implements Callable<String> {
    private String request;
    private String filename;

    public GetRate(String request, String filename) {
        this.request = request;
        this.filename = filename;
    }

    @Override
    public String call() throws InterruptedException {
        CacheManager cache = new CacheManager(filename);
        String rate = "";
        try {
            if (cache.checkCache()) {
                rate = cache.readCache();
            } else {
                GetResponse response = new GetResponse(request);
                rate = response.getRate();
                cache.createCache(rate);
            }
        } catch (IOException e) {
            System.out.println("I/O error: " + e);
        }
        return rate;
    }
}
