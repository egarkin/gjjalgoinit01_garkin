import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetResponse {
    private URL request;
    private HttpURLConnection connection;
    private BufferedReader reader;
    private ApiResponse response;
    private RateObject rates;

    public GetResponse(String request) throws IOException {
        try {
            this.request = new URL(request);
            connection = (HttpURLConnection) this.request.openConnection();
            int responseCode = connection.getResponseCode();
            if (responseCode >= 200 && responseCode <= 299) {
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                Gson gson = new GsonBuilder().registerTypeAdapter(RateObject.class, new RatesDeserializer()).create();
                response = gson.fromJson(reader, ApiResponse.class);
                rates = response.getRates();
            } else {
                System.out.println("Something went wrong! HTTP Code: " + responseCode);
            }
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    System.out.println("I/O error: " + e);
                }
            }
        }
    }

    public String getRate() {
        return String.valueOf(rates.getRate());
    }
}
