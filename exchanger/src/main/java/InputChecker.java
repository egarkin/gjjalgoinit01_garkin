import java.util.*;

public class InputChecker {
    private static final Set<String> supportedCurrencies = new HashSet<>(Arrays.asList(
            "AUD", "BGN", "BRL", "CAD", "CHF", "CNY", "CZK", "DKK",
            "EUR", "GBP", "HKD", "HRK", "HUF", "IDR", "ILS", "INR",
            "JPY", "KRW", "MXN", "MYR", "NOK", "NZD", "PHP", "PLN",
            "RON", "RUB", "SEK", "SGD", "THB", "TRY", "USD", "ZAR"));

    public static boolean check(String currency) {
        if (!supportedCurrencies.contains(currency)) {
            System.out.println("Input currency " + currency + " is not supported");
            return false;
        } else {
            return true;
        }
    }
}
