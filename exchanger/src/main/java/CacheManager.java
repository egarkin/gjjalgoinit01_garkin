import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class CacheManager {
    private String filePath;

    public CacheManager(String filename) {
        filePath = System.getProperty("user.dir") + "\\cache\\" + filename;
    }

    public boolean checkCache() {
        File file = new File(filePath);
        return file.exists();
    }

    public void createCache(String rate) throws IOException {
        Path path = Paths.get(filePath);
        Files.createDirectories(path.getParent());
        Files.write(path, rate.getBytes(), StandardOpenOption.CREATE);
    }


    public String readCache() throws IOException {
        Path path = Paths.get(filePath);
        String rate = new String(Files.readAllBytes(path));
        return rate;
    }
}
