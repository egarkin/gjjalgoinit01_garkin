public class RateObject {
    private String name;
    private double rate;

    protected double getRate() {
        return rate;
    }

    public RateObject(String name, double rate) {
        this.name = name;
        this.rate = rate;
    }
}
