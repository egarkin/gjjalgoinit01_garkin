package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 03.07.2016.
 */
public class TaskCh02N013 {
    public static void main(String[] args) {
        System.out.println("Input number between 100 and 200");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        if (number > 100 || number < 200) {
            int reversedNumber = reverseNumber(number);
            System.out.println("Reversed number = " + reversedNumber);
        } else {
            System.out.println("Input out of range");
        }
    }

    public static int reverseNumber(int number) {
        int hundreds = number / 100;
        int tens = number / 10 % 10;
        int ones = number % 10;

        return ones * 100 + tens * 10 + hundreds;
    }
}
