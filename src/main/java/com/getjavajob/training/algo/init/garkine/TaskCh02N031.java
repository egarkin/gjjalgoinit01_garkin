package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 03.07.2016.
 */
public class TaskCh02N031 {
    public static void main(String[] args) {
        System.out.println("Input number with 3 digits");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        if (number >= 100 || number <= 999) {
            int result = findNumber(number);
            System.out.println("result = " + result);
        } else {
            System.out.println("Input out of range");
        }
    }

    public static int findNumber(int number) {
        int hundreds = number / 100;
        int tens = number / 10 % 10;
        int ones = number % 10;

        return hundreds * 100 + ones * 10 + tens;
    }
}