package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh06N087Test {
    public static void main(String[] args) {
        testGame();
        testIntermediateResults();
        testIntermediateTeamPoints();
    }

    public static void testGame() {
        Game game = new Game();
        game.firstTeam = new Team("Spartak");
        game.secondTeam = new Team("cska");
        game.firstTeam.increaseTeamPoints(3);
        game.secondTeam.increaseTeamPoints(1);
        game.firstTeam.increaseTeamPoints(3);
        assertEquals("TaskCh06N087Test.testGame", "Spartak wins with score 6 : 1", game.result(true));
    }

    public static void testIntermediateResults() {
        Game game = new Game();
        game.firstTeam = new Team("Spartak");
        game.secondTeam = new Team("cska");
        game.firstTeam.increaseTeamPoints(3);
        game.secondTeam.increaseTeamPoints(1);
        assertEquals("TaskCh06N087Test.testIntermediateResults", "Spartak 3 : 1 cska", game.result(false));
    }

    public static void testIntermediateTeamPoints() {
        Game game = new Game();
        game.firstTeam = new Team("Spartak");
        game.firstTeam.increaseTeamPoints(3);
        assertEquals("TaskCh06N087Test.testIntermediateTeamPoints", 3, game.firstTeam.getTeamPoints());
    }
}
