package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 04.07.2016.
 */
public class TaskCh06N087 {
    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}

class Team {
    private String name;
    private int teamPoints;

    public Team(String name) {
        this.name = name;
        teamPoints = 0;
    }

    public String getName() {
        return name;
    }

    public int getTeamPoints() {
        return teamPoints;
    }

    public void increaseTeamPoints(int amount) {
        teamPoints += amount;
    }
}

class Game {
    public Team firstTeam;
    public Team secondTeam;

    public void play() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter team #1:");
        firstTeam = new Team(scanner.nextLine());
        System.out.println("Enter team #2:");
        secondTeam = new Team(scanner.nextLine());
        boolean isGameOver = false;
        while (!isGameOver) {
            System.out.println("Enter team to score (1 or 2 or 0 to finish game):");
            int teamNumber = scanner.nextInt();
            if (teamNumber == 0) {
                isGameOver = true;
            } else {
                System.out.println("Enter points (1 or 2 or 3):");
                if (teamNumber == 1) {
                    firstTeam.increaseTeamPoints(scanner.nextInt());
                    System.out.println(result(isGameOver));
                } else if (teamNumber == 2) {
                    secondTeam.increaseTeamPoints(scanner.nextInt());
                    System.out.println(result(isGameOver));
                }
            }
        }
        System.out.println(result(isGameOver));
    }

    public String result(boolean isGameOver) {
        if (isGameOver) {
            if (firstTeam.getTeamPoints() > secondTeam.getTeamPoints()) {
                return firstTeam.getName() + " wins with score " + firstTeam.getTeamPoints()
                        + " : " + secondTeam.getTeamPoints();
            } else if (secondTeam.getTeamPoints() > firstTeam.getTeamPoints()) {
                return secondTeam.getName() + " wins with score " + secondTeam.getTeamPoints()
                        + " : " + firstTeam.getTeamPoints();
            } else {
                return "Draw " + firstTeam.getTeamPoints() + " : " + secondTeam.getTeamPoints();
            }
        } else {
            return firstTeam.getName() + " " + firstTeam.getTeamPoints() + " : "
                    + secondTeam.getTeamPoints() + " " + secondTeam.getName();
        }
    }
}