package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh05N038.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh05N038Test {
    public static void main(String[] args) {
        testCalculateDistance();
        testCalculatePath();
    }

    public static void testCalculateDistance() {
        assertEquals("TaskCh03N026Test.testCalculateDistance", 0.5, calculateDistance(2));
    }

    public static void testCalculatePath() {
        assertEquals("TaskCh03N026Test.testCalculatePath", 1.5, calculatePath(2));
    }
}
