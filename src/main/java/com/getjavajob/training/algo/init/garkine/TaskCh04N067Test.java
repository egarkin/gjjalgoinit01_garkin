package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh04N067.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh04N067Test {
    public static void main(String[] args) {
        testWorkday();
        testWeekend();
    }

    public static void testWorkday() {
        assertEquals("TaskCh04N067Test.testWorkday", "Workday", calculateDay(5));
    }

    public static void testWeekend() {
        assertEquals("TaskCh04N067Test.testWeekend", "Weekend", calculateDay(7));
    }
}
