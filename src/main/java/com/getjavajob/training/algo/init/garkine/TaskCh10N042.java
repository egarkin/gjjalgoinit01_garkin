package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 07.07.2016.
 */
public class TaskCh10N042 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input number");
        double number = scanner.nextDouble();
        System.out.println("Input power");
        int power = scanner.nextInt();

        double result = pow(number, power);
        System.out.println(number + " in power " + power + " is " + result);
    }

    public static double pow(double number, int power) {
        if (power == 1) {
            return number;
        } else {
            return number * pow(number, power - 1);
        }
    }
}