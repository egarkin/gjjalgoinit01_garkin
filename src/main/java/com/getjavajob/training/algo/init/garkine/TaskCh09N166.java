package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 06.07.2016.
 */
public class TaskCh09N166 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input sentence");
        String sentence = scanner.nextLine();

        String result = swapWords(sentence);
        System.out.println("Sentence after swap words \"" + result + "\"");
    }

    public static String swapWords(String sentence) {
        String[] words = sentence.split(" ");
        String temp = words[0];
        words[0] = words[words.length - 1];
        words[words.length - 1] = temp;
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < words.length; i++) {
            builder.append(words[i]);
            if (i != words.length - 1) {
                builder.append(" ");
            }
        }
        return builder.toString();
    }
}