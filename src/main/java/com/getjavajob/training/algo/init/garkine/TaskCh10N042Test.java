package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N042.pow;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh10N042Test {
    public static void main(String[] args) {
        testPow();
    }

    public static void testPow() {
        assertEquals("TaskCh10N042Test.testPow", 8, pow(2, 3));
    }
}
