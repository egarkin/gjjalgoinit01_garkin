package com.getjavajob.training.algo.init.garkine;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Evgeny on 10.07.2016.
 */
public class TaskCh12N023 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input array length. Length must be odd.");
        int length = scanner.nextInt();

        int[][] resultA = taskA(new int[length][length]);
        int[][] resultB = taskB(new int[length][length]);
        int[][] resultC = taskC(new int[length][length]);
        System.out.println("Task A:\n" + Arrays.deepToString(resultA));
        System.out.println("\nTask B:\n" + Arrays.deepToString(resultB));
        System.out.println("\nTask C:\n" + Arrays.deepToString(resultC));
    }

    public static int[][] taskA(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            array[i][i] = 1;
        }
        for (int i = 0; i < array.length; i++) {
            array[i][array.length - 1 - i] = 1;
        }
        return array;
    }

    public static int[][] taskB(int[][] array) {
        taskA(array);
        for (int i = 0; i < array.length; i++) {
            array[i][array.length / 2] = 1;
            array[array.length / 2][i] = 1;
        }
        return array;
    }

    public static int[][] taskC(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            if (i <= array.length / 2) {
                for (int j = i; j < array.length - i; j++) {
                    array[i][j] = 1;
                }
            } else {
                for (int j = array.length - 1 - i; j <= i; j++) {
                    array[i][j] = 1;
                }
            }
        }
        return array;
    }
}