package com.getjavajob.training.algo.init.garkine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import java.util.Scanner;

/**
 * Created by Johnny on 11.07.2016.
 */
public class TaskCh13N012 {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Input number of employees");
        int numberOfEmployees = scanner.nextInt();
        Database employeesData = new Database();
        for (int i = 0; i < numberOfEmployees; i++) {
            System.out.println("Input " + (i + 1) + " employee data:");
            System.out.println("Input First name");
            String firstName = scanner.next();
            System.out.println("Input Last name");
            String lastName = scanner.next();
            System.out.println("Input Patronymic");
            String patronymic = reader.readLine();
            System.out.println("Input Address");
            String address = scanner.next();
            System.out.println("Input month joining");
            int monthJoining = scanner.nextInt();
            System.out.println("Input year joining");
            int yearJoining = scanner.nextInt();
            if (patronymic.isEmpty()) {
                Employee employee = new Employee(firstName, lastName, address,
                        monthJoining, yearJoining);
                employeesData.addEmployee(employee);
            } else {
                Employee employee = new Employee(firstName, lastName, patronymic,
                        address, monthJoining, yearJoining);
                employeesData.addEmployee(employee);
            }
        }

        System.out.println("Input number of working years");
        int numberOfWorkingYears = scanner.nextInt();
        Calendar calendar = Calendar.getInstance();
        int monthNow = calendar.get(Calendar.MONTH);
        int yearNow = calendar.get(Calendar.YEAR);

        employeesData.outputResults(employeesData.
                employeesWhoWorkMoreThanInputYears(numberOfWorkingYears, monthNow, yearNow));

        System.out.println("\nInput string to find");
        String stringToFind = scanner.next();
        employeesData.outputResults(employeesData.employeesWithInputSubstring(stringToFind));
    }
}

class Employee {
    private String firstName;
    private String lastName;
    private String patronymic = "";
    private String address;
    private int monthJoining;
    private int yearJoining;

    public Employee(String firstName, String lastName, String address, int monthJoining, int yearJoining) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.monthJoining = monthJoining;
        this.yearJoining = yearJoining;
    }

    public Employee(String firstName, String lastName, String patronymic, String address,
                    int monthJoining, int yearJoining) {
        this(firstName, lastName, address, monthJoining, yearJoining);
        this.patronymic = patronymic;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getAddress() {
        return address;
    }

    public int getNumberOfWorkingYears(int monthNow, int yearNow) {
        int years = yearNow - yearJoining;
        return monthJoining <= monthNow ? years : --years;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (monthJoining != employee.monthJoining) return false;
        if (yearJoining != employee.yearJoining) return false;
        if (!firstName.equals(employee.firstName)) return false;
        if (!lastName.equals(employee.lastName)) return false;
        return patronymic != null ? patronymic.equals(employee.patronymic)
                : employee.patronymic == null && address.equals(employee.address);

    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        result = 31 * result + address.hashCode();
        result = 31 * result + monthJoining;
        result = 31 * result + yearJoining;
        return result;
    }
}

class Database {
    private List<Employee> employeesData;

    public Database() {
        employeesData = new ArrayList<>();
    }

    public void outputResults(List<Employee> employeesData) {
        if (employeesData.isEmpty()) {
            System.out.println("No one employee founded");
        } else {
            for (Employee employee : employeesData) {
                if (employee.getPatronymic().isEmpty()) {
                    System.out.println(employee.getLastName() + "\t" + employee.getFirstName() + "\t"
                            + employee.getAddress());
                } else {
                    System.out.println(employee.getLastName() + "\t" + employee.getFirstName() + "\t"
                            + employee.getPatronymic() + "\t" + employee.getAddress());
                }
            }
        }
    }

    public void addEmployee(Employee employee) {
        employeesData.add(employee);
    }

    public List<Employee> employeesWithInputSubstring(String stringToFind) {
        List<Employee> foundedEmployees = new ArrayList<>();
        for (Employee employee : employeesData) {
            if (employee.getFirstName().toLowerCase().contains(stringToFind.toLowerCase())
                    || employee.getLastName().toLowerCase().contains(stringToFind.toLowerCase())
                    || employee.getPatronymic().toLowerCase().contains(stringToFind.toLowerCase())) {
                foundedEmployees.add(employee);
            }
        }
        return foundedEmployees;
    }

    public List<Employee> employeesWhoWorkMoreThanInputYears(int numberOfWorkedYears, int monthNow, int yearNow) {
        List<Employee> foundedEmployees = new ArrayList<>();
        for (Employee employee : employeesData) {
            if (employee.getNumberOfWorkingYears(monthNow, yearNow)
                    >= numberOfWorkedYears) {
                foundedEmployees.add(employee);
            }
        }
        return foundedEmployees;
    }
}