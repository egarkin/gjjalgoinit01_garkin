package com.getjavajob.training.algo.init.garkine;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Evgeny on 10.07.2016.
 */
public class TaskCh12N028 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean isInputCorrectly = false;
        int length = 0;
        while (!isInputCorrectly) {
            System.out.println("Input array length, where length % 2 = 1");
            length = scanner.nextInt();
            if (length % 2 == 1) {
                isInputCorrectly = true;
            } else {
                System.out.println("Length % 2 must be equals 1!\n");
            }
        }

        int[][] result = fillArray(new int[length][length]);
        System.out.println(Arrays.deepToString(result));
    }

    public static int[][] fillArray(int[][] array) {
        int value = 0;

        for (int k = 0; k <= array.length / 2; k++) {
            for (int j = k; j < array.length - k; j++) {
                array[k][j] = ++value;
            }
            for (int i = k + 1; i < array.length - k; i++) {
                array[i][array.length - k - 1] = ++value;
            }
            for (int j = array.length - k - 2; j >= k; j--) {
                array[array.length - k - 1][j] = ++value;
            }
            for (int i = array.length - k - 2; i >= k + 1; i--) {
                array[i][k] = ++value;
            }
        }
        return array;
    }
}