package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 04.07.2016.
 */
public class TaskCh04N067 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input number of day");
        int day = scanner.nextInt();

        String result = calculateDay(day);
        System.out.println("Day number " + day + " is " + result);
    }

    public static String calculateDay(int day) {
        return day % 7 == 0 || day % 7 == 6 ? "Weekend" : "Workday";
    }
}