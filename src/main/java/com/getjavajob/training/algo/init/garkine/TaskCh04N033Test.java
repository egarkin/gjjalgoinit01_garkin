package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh04N033.isEven;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh04N033Test {
    public static void main(String[] args) {
        testBNumberEndsOnOddDigit();
        testNumberEndsOnEvenDigit();
    }

    public static void testNumberEndsOnEvenDigit() {
        assertEquals("TaskCh03N026Test.testNumberEndsOnEvenDigit", true, isEven(42));
    }

    public static void testBNumberEndsOnOddDigit() {
        assertEquals("TaskCh03N026Test.testNumberEndsOnOddDigit", false, isEven(41));
    }
}
