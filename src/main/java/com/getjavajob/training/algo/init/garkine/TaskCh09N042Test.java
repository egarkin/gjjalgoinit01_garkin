package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh09N042.reverseWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh09N042Test {
    public static void main(String[] args) {
        testReverseWord();
    }

    public static void testReverseWord() {
        assertEquals("TaskCh09N042Test.testReverseWord", "tseT", reverseWord("Test"));
    }
}
