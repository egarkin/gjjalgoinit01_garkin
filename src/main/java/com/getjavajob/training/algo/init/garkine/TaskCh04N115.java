package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 04.07.2016.
 */
public class TaskCh04N115 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input year");
        int year = scanner.nextInt();

        String result = calculateYear(year);
        System.out.println(year + " year is " + result);
    }

    public static String calculateYear(int year) {
        String color;
        switch (year % 10) {
            case 0:
            case 1:
                color = "White ";
                break;
            case 2:
            case 3:
                color = "Black ";
                break;
            case 4:
            case 5:
                color = "Green ";
                break;
            case 6:
            case 7:
                color = "Red ";
                break;
            case 8:
            case 9:
                color = "Yellow ";
                break;
            default:
                color = "";
                break;
        }
        switch (year % 12) {
            case 0:
                return color + "Monkey";
            case 1:
                return color + "Cock";
            case 2:
                return color + "Dog";
            case 3:
                return color + "Pig";
            case 4:
                return color + "Rat";
            case 5:
                return color + "Cow";
            case 6:
                return color + "Tiger";
            case 7:
                return color + "Rabbit";
            case 8:
                return color + "Dragon";
            case 9:
                return color + "Snake";
            case 10:
                return color + "Horse";
            case 11:
                return color + "Sheep";
            default:
                return "";
        }
    }
}