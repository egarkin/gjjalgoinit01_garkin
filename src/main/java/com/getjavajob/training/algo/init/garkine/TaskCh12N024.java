package com.getjavajob.training.algo.init.garkine;

import java.util.Arrays;
import java.util.Scanner;

import static com.getjavajob.training.algo.init.garkine.TaskCh12N024.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Evgeny on 10.07.2016.
 */
public class TaskCh12N024 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input array length");
        int length = scanner.nextInt();

        int[][] resultA = taskA(new int[length][length]);
        int[][] resultB = taskB(new int[length][length]);
        System.out.println("Task A:\n" + Arrays.deepToString(resultA));
        System.out.println("\nTask B:\n" + Arrays.deepToString(resultB));
    }

    public static int[][] taskA(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (i == 0 || j == 0) {
                    array[i][j] = 1;
                } else {
                    array[i][j] = array[i - 1][j] + array[i][j - 1];
                }
            }
        }
        return array;
    }

    public static int[][] taskB(int[][] array) {
        int[] numbers = new int[]{1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6};
        for (int i = 0; i < array.length; i++) {
            System.arraycopy(numbers, i, array[i], 0, array.length);
        }
        return array;
    }
}