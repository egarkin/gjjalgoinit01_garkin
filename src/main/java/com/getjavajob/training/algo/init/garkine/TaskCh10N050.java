package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 07.07.2016.
 */
public class TaskCh10N050 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input N");
        int n = scanner.nextInt();
        System.out.println("Input M");
        int m = scanner.nextInt();

        int result = ackermann(n, m);
        System.out.println("Ackermann function is " + result);
    }

    public static int ackermann(int n, int m) {
        if (n == 0) {
            return ++m;
        } else {
            if (m == 0) {
                return ackermann(n - 1, 1);
            } else {
                return ackermann(n - 1, ackermann(n, m - 1));
            }
        }
    }
}