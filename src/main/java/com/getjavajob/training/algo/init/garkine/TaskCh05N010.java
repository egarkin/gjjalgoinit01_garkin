package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 04.07.2016.
 */
public class TaskCh05N010 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input exchange rate");
        double rate = scanner.nextDouble();

        System.out.println("Dollars\tRubles");
        double[] result = calculateRubles(rate);
        printArray(result);
    }

    public static double[] calculateRubles(double rate) {
        double[] exchangeTable = new double[20];
        for (int i = 0; i < exchangeTable.length; i++) {
            exchangeTable[i] = (i + 1) * rate;
        }
        return exchangeTable;
    }

    public static void printArray(double[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(i + 1 + "\t\t" + array[i]);
        }
    }
}