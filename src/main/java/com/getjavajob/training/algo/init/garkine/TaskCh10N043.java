package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 07.07.2016.
 */
public class TaskCh10N043 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input number");
        int number = scanner.nextInt();

        int sumDigits = sumDigits(number);
        int amountDigits = amountDigits(number);
        System.out.println("Sum of digits is " + sumDigits);
        System.out.println("Digit`s amount is " + amountDigits);
    }

    public static int sumDigits(int number) {
        if (number == 0) {
            return 0;
        } else {
            return number % 10 + sumDigits(number / 10);
        }
    }

    public static int amountDigits(int number) {
        if (number >= 0 && number <= 9) {
            return 1;
        } else {
            return amountDigits(number / 10) + 1;
        }
    }
}