package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh04N036.findColor;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh04N036Test {
    public static void main(String[] args) {
        testGreenTrafficLight();
        testRedTrafficLight();
    }

    public static void testGreenTrafficLight() {
        assertEquals("TaskCh03N026Test.testGreenTrafficLight", "Green", findColor(5));
    }

    public static void testRedTrafficLight() {
        assertEquals("TaskCh03N026Test.testRedTrafficLight", "Red", findColor(3));
    }
}
