package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh06N008.findNumbersLessN;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh06N008Test {
    public static void main(String[] args) {
        testFindNumberLessN();
    }

    public static void testFindNumberLessN() {
        int[] expected = {1, 4, 9};
        assertEquals("TaskCh06N008Test.testFindNumberLessN", expected, findNumbersLessN(15));
    }
}
