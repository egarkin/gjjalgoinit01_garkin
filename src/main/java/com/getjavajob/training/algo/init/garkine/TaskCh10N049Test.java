package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N049.findIndexOfMaxElement;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh10N049Test {
    public static void main(String[] args) {
        testFindIndexOfMaxElement();
    }

    public static void testFindIndexOfMaxElement() {
        int[] actual = {1, 4, 8, 3, 6};
        assertEquals("TaskCh10N049Test.testFindIndexOfMaxElement", 2,
                findIndexOfMaxElement(actual, 5));
    }
}