package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N046.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh10N046Test {
    public static void main(String[] args) {
        testCalculateTerm();
        testCalculateSumOfTerms();
    }

    public static void testCalculateTerm() {
        assertEquals("TaskCh10N046Test.testCalculateTerm", 8, calculateTerm(2, 2, 3));
    }

    public static void testCalculateSumOfTerms() {
        assertEquals("TaskCh10N046Test.testCalculateSumOfTerms", 30, calculateSumOfTerms(2, 2, 4));
    }
}
