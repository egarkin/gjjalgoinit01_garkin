package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh09N185.checkExtraBraces;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh09N185Test {
    public static void main(String[] args) {
        testExtraRightBraces();
        testExtraLeftBraces();
        testBracesPlacedCorrectly();
    }

    public static void testExtraRightBraces() {
        assertEquals("TaskCh09N185Test.testExtraRightBraces",
                "No, braces placed incorrectly. First redundant \")\" at position 3", checkExtraBraces("()))"));
    }

    public static void testExtraLeftBraces() {
        assertEquals("TaskCh09N185Test.testExtraLeftBraces",
                "No, braces placed incorrectly. Number of redundant \"(\" is 2", checkExtraBraces("((()"));
    }

    public static void testBracesPlacedCorrectly() {
        assertEquals("TaskCh09N185Test.testBracesPlacedCorrectly",
                "Yes, braces placed correctly!", checkExtraBraces("(())"));
    }
}