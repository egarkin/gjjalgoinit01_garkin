package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh09N166.swapWords;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh09N166Test {
    public static void main(String[] args) {
        testSwapWords();
    }

    public static void testSwapWords() {
        assertEquals("TaskCh09N166Test.testSwapWords", "world Hello", swapWords("Hello world"));
    }
}