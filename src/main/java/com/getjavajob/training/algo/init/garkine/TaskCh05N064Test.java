package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh05N064.calculateAveragePopulation;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh05N064Test {
    public static void main(String[] args) {
        testCalculateAveragePopulation();
    }

    public static void testCalculateAveragePopulation() {
        int[][] input = new int[][]{
                {10, 5},
                {12, 6},
                {16, 8}};
        assertEquals("TaskCh05N064Test.testCalculateAveragePopulation", 6, calculateAveragePopulation(input));
    }
}
