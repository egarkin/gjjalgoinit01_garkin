package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 03.07.2016.
 */
public class TaskCh02N039 {
    static final int HOURS_PER_DAY = 24;
    static final int MINUTES_PER_HOUR = 60;
    static final int SECONDS_PER_MINUTES = 60;
    static final int SECONDS_PER_HOUR = 3600;
    static final int DEGREES_IN_CIRCLE = 360;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input hours");
        int hours = scanner.nextInt();
        System.out.println("Input minutes");
        int minutes = scanner.nextInt();
        System.out.println("Input seconds");
        int seconds = scanner.nextInt();
        if (hours > 0 && hours < HOURS_PER_DAY && minutes >= 0 && minutes < MINUTES_PER_HOUR &&
                seconds >= 0 && seconds < SECONDS_PER_MINUTES) {
            double result = calculateAngle(hours, minutes, seconds);
            System.out.println("Angle is " + result + " degrees");
        } else {
            System.out.println("Input out of range");
        }
    }

    public static double calculateAngle(int hours, int minutes, int seconds) {
        return (hours + ((double) minutes / MINUTES_PER_HOUR) + ((double) seconds / SECONDS_PER_HOUR))
                * DEGREES_IN_CIRCLE / HOURS_PER_DAY;
    }
}
