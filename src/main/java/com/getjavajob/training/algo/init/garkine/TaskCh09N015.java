package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 06.07.2016.
 */
public class TaskCh09N015 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input word");
        String word = scanner.nextLine();
        boolean isOutOfRange = true;
        int number = 0;
        while (isOutOfRange) {
            System.out.println("Input symbol`s number to find");
            number = scanner.nextInt();
            if ((word.length() + 1 > number) && (number > 0)) {
                isOutOfRange = false;
            } else {
                System.out.println("Number is out of range word length\n");
            }
        }

        char result = findSymbol(word, number);
        System.out.println("Symbol at " + number + " position is \"" + result + "\"");
    }

    public static char findSymbol(String word, int number) {
        return word.charAt(number - 1);
    }
}