package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 04.07.2016.
 */
public class TaskCh02N043 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input first number");
        int firstNumber = scanner.nextInt();
        System.out.println("Input second number");
        int secondNumber = scanner.nextInt();

        int result = isDivisible(firstNumber, secondNumber);
        System.out.println(result);
    }

    public static int isDivisible(int firstNumber, int secondNumber) {
        if (firstNumber == 0 || secondNumber == 0) {
            return 0;
        } else {
            return firstNumber % secondNumber + secondNumber % firstNumber + 1;
        }
    }
}