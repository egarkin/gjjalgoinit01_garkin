package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N050.ackermann;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh10N050Test {
    public static void main(String[] args) {
        testAckermannNEqualsZero();
        testAckermannMEqualsZero();
        testAckermann();
    }

    public static void testAckermannNEqualsZero() {
        assertEquals("TaskCh10N050Test.testAckermannNEqualsZero", 3, ackermann(0, 2));
    }

    public static void testAckermannMEqualsZero() {
        assertEquals("TaskCh10N050Test.testAckermannMEqualsZero", 5, ackermann(3, 0));
    }

    public static void testAckermann() {
        assertEquals("TaskCh10N050Test.testAckermann", 5, ackermann(1, 3));
    }
}
