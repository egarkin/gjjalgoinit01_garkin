package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh03N029.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh03N029Test {
    public static void main(String[] args) {
        testTaskAOneNumberEven();
        testTaskATwoNumbersEven();
        testTaskATwoNumbersNotEven();
        testTaskBOneNumberLessTwenty();
        testTaskBTwoNumbersLessTwenty();
        testTaskBTwoNumbersLargerTwenty();
        testTaskCOneNumberEqualsZero();
        testTaskCTwoNumbersEqualsZero();
        testTaskCTwoNumbersNotEqualsZero();
        testTaskDOnePositiveNumber();
        testTaskDTwoPositiveNumbers();
        testTaskDThreePositiveNumbers();
        testTaskDThreeNumbersLessZero();
        testTaskENoOneNumberMultipleFive();
        testTaskEOneNumberInFirstPositionMultipleFive();
        testTaskEOneNumberInSecondPositionMultipleFive();
        testTaskEOneNumberInThirdPositionMultipleFive();
        testTaskETwoNumbersInFirstAndSecondPositionMultipleFive();
        testTaskETwoNumbersInSecondAndThirdPositionMultipleFive();
        testTaskETwoNumbersInSecondAndThirdPositionMultipleFive();
        testTaskEThreeNumbersMultipleFive();
        testTaskFNoOneNumberMoreThanHundred();
        testTaskFOneNumberMoreThanHundred();
        testTaskFTwoNumbersMoreThanHundred();
        testTaskFThreeNumbersMoreThanHundred();
    }

    public static void testTaskATwoNumbersEven() {
        assertEquals("TaskCh03N029Test.testTaskATwoNumbersEven", false, taskA(4, 2));
    }

    public static void testTaskAOneNumberEven() {
        assertEquals("TaskCh03N029Test.testTaskAOneNumberEven", false, taskA(6, 5));
    }

    public static void testTaskATwoNumbersNotEven() {
        assertEquals("TaskCh03N029Test.testTaskATwoNumbersNotEven", true, taskA(3, 1));
    }

    public static void testTaskBOneNumberLessTwenty() {
        assertEquals("TaskCh03N029Test.testTaskBOneNumberLessTwenty", true, taskB(19, 21));
    }

    public static void testTaskBTwoNumbersLessTwenty() {
        assertEquals("TaskCh03N029Test.testTaskBTwoNumbersLessTwenty", false, taskB(19, 18));
    }

    public static void testTaskBTwoNumbersLargerTwenty() {
        assertEquals("TaskCh03N029Test.testTaskBTwoNumbersLargerTwenty", false, taskB(23, 21));
    }

    public static void testTaskCOneNumberEqualsZero() {
        assertEquals("TaskCh03N029Test.testTaskCOneNumberEqualsZero", true, taskC(0, 13));
    }

    public static void testTaskCTwoNumbersEqualsZero() {
        assertEquals("TaskCh03N029Test.testTaskCTwoNumbersEqualsZero", true, taskC(0, 0));
    }

    public static void testTaskCTwoNumbersNotEqualsZero() {
        assertEquals("TaskCh03N029Test.testTaskCTwoNumbersNotEqualsZero", false, taskC(14, 19));
    }

    public static void testTaskDThreePositiveNumbers() {
        assertEquals("TaskCh03N029Test.testTaskDThreePositiveNumbers", false, taskD(3, 5, 7));
    }

    public static void testTaskDTwoPositiveNumbers() {
        assertEquals("TaskCh03N029Test.testTaskDTwoPositiveNumbers", false, taskD(-20, 2, 9));
    }

    public static void testTaskDOnePositiveNumber() {
        assertEquals("TaskCh03N029Test.testTaskDOnePositiveNumber", false, taskD(-9, 4, -4));
    }

    public static void testTaskDThreeNumbersLessZero() {
        assertEquals("TaskCh03N029Test.testTaskDThreeNumberLessZero", true, taskD(-38, -9, -76));
    }

    public static void testTaskEOneNumberInFirstPositionMultipleFive() {
        assertEquals("TaskCh03N029Test.testTaskEOneNumberInFirstPositionMultipleFive", true, taskE(5, 4, 3));
    }

    public static void testTaskEOneNumberInSecondPositionMultipleFive() {
        assertEquals("TaskCh03N029Test.testTaskEOneNumberInSecondPositionMultipleFive", true, taskE(3, 5, 4));
    }

    public static void testTaskEOneNumberInThirdPositionMultipleFive() {
        assertEquals("TaskCh03N029Test.testTaskEOneNumberInThirdPositionMultipleFive", true, taskE(3, 4, 5));
    }

    public static void testTaskETwoNumbersInFirstAndSecondPositionMultipleFive() {
        assertEquals("TaskCh03N029Test.testTaskETwoNumbersInFirstAndSecondPositionMultipleFive",
                false, taskE(5, 10, 2));
    }

    public static void testTaskETwoNumbersInFirstAndThirdPositionMultipleFive() {
        assertEquals("TaskCh03N029Test.testTaskETwoNumbersInFirstAndThirdPositionMultipleFive",
                false, taskE(5, 2, 10));
    }

    public static void testTaskETwoNumbersInSecondAndThirdPositionMultipleFive() {
        assertEquals("TaskCh03N029Test.testTaskETwoNumbersInSecondAndThirdPositionMultipleFive",
                false, taskE(2, 10, 5));
    }

    public static void testTaskEThreeNumbersMultipleFive() {
        assertEquals("TaskCh03N029Test.testTaskEThreeNumbersMultipleFive", false, taskE(5, 10, 15));
    }

    public static void testTaskENoOneNumberMultipleFive() {
        assertEquals("TaskCh03N029Test.testTaskENoOneNumberMultipleFive", false, taskE(7, 9, 12));
    }

    public static void testTaskFNoOneNumberMoreThanHundred() {
        assertEquals("TaskCh03N029Test.testTaskFNoOneNumberMoreThanHundred", false, taskF(90, 32, 54));
    }

    public static void testTaskFOneNumberMoreThanHundred() {
        assertEquals("TaskCh03N029Test.testTaskFOneNumberMoreThanHundred", true, taskF(132, 59, 76));
    }

    public static void testTaskFTwoNumbersMoreThanHundred() {
        assertEquals("TaskCh03N029Test.testTaskFTwoNumbersMoreThanHundred", true, taskF(154, 12, 198));
    }

    public static void testTaskFThreeNumbersMoreThanHundred() {
        assertEquals("TaskCh03N029Test.testTaskFNoOneNumberMoreThanHundred", true, taskF(176, 121, 854));
    }
}
