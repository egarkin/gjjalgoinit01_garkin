package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 04.07.2016.
 */
public class TaskCh04N033 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input natural number");
        int number = scanner.nextInt();

        System.out.println(isEven(number) ? "Number ends in even digit" : "Number ends in odd digit");
    }

    public static boolean isEven(int number) {
        return number % 2 == 0;
    }
}
