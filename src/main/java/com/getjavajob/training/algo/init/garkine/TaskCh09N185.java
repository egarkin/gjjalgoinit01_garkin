package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 06.07.2016.
 */
public class TaskCh09N185 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input expression");
        String expression = scanner.nextLine();

        String result = checkExtraBraces(expression);
        System.out.println(result);
    }

    public static String checkExtraBraces(String expression) {
        int leftBraces = 0;

        for (int i = 0; i < expression.length(); i++) {
            switch (expression.charAt(i)) {
                case '(':
                    leftBraces++;
                    break;
                case ')':
                    if (leftBraces == 0) {
                        return "No, braces placed incorrectly. First redundant \")\" at position " + (i + 1);
                    } else {
                        leftBraces--;
                    }
                    break;
                default:
                    break;
            }
        }
        if (leftBraces == 0) {
            return "Yes, braces placed correctly!";
        } else {
            return "No, braces placed incorrectly. Number of redundant \"(\" is " + leftBraces;
        }
    }
}