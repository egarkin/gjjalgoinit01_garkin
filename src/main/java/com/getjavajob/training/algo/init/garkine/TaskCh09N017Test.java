package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh09N017.isSameLetter;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh09N017Test {
    public static void main(String[] args) {
        testWordBeginsAndEndsDifferentLetters();
        testWordBeginsAndEndsSameLetter();
    }

    public static void testWordBeginsAndEndsSameLetter() {
        assertEquals("TaskCh09N015Test.testBeginsAndEndsSameLetter", true, isSameLetter("Test"));
    }

    public static void testWordBeginsAndEndsDifferentLetters() {
        assertEquals("TaskCh09N015Test.testWordBeginsAndEndsDifferentLetters", false, isSameLetter("Symbol"));
    }
}
