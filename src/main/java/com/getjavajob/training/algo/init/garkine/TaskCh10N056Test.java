package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N056.primalityTest;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh10N056Test {
    public static void main(String[] args) {
        testPrimeNumber();
        testNotPrimeNumber();
    }

    public static void testPrimeNumber() {
        assertEquals("TaskCh10N056Test.testPrimeNumber", "a prime", primalityTest(13, 2));
    }

    public static void testNotPrimeNumber() {
        assertEquals("TaskCh10N056Test.testNotPrimeNumber", "not a prime", primalityTest(15, 2));
    }
}
