package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 06.07.2016.
 */
public class TaskCh09N022 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean isEven = false;
        String word = "";
        while (!isEven) {
            System.out.println("Input word with even number of symbols");
            word = scanner.nextLine();
            if (word.length() % 2 == 0) {
                isEven = true;
            } else {
                System.out.println("Word \"" + word + "\"has odd number of symbols\n");
            }
        }

        String result = wordFirstPart(word);
        System.out.println("The first half part of word \"" + word + "\" is \"" + result + "\"");
    }

    public static String wordFirstPart(String word) {
        return word.substring(0, word.length() / 2);
    }
}