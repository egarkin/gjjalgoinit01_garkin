package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 07.07.2016.
 */
public class TaskCh10N048 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input array size");
        int size = scanner.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            System.out.println("Input " + (i + 1) + " element of array");
            array[i] = scanner.nextInt();
        }

        int result = findMax(array, size);
        System.out.println("Maximum element is " + result);
    }

    public static int findMax(int[] array, int size) {
        if (size == 0) {
            return array[0];
        } else {
            int maximum = findMax(array, size - 1);
            return array[size - 1] > maximum ? array[size - 1] : maximum;
        }
    }
}