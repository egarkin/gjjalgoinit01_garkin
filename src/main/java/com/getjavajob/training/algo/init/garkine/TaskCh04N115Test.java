package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh04N115.calculateYear;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh04N115Test {
    public static void main(String[] args) {
        testCalculateYearGreenRat();
        testCalculateYearLessThan1984();
    }

    public static void testCalculateYearGreenRat() {
        assertEquals("TaskCh03N026Test.testCalculateYearGreenRat", "Green Rat", calculateYear(1984));
    }

    public static void testCalculateYearLessThan1984() {
        assertEquals("TaskCh03N026Test.testCalculateYearLessThan1984", "Black Dragon", calculateYear(1952));
    }
}
