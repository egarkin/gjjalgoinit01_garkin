package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N052.reverseDigits;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh10N052Test {
    public static void main(String[] args) {
        testReverseDigits();
    }

    public static void testReverseDigits() {
        assertEquals("TaskCh10N052Test.testReverseDigits", 123, reverseDigits(321));
    }
}