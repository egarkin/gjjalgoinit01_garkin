package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 04.07.2016.
 */
public class TaskCh06N008 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input N");
        int number = scanner.nextInt();

        int[] numbers = findNumbersLessN(number);
        printArray(numbers);
    }

    public static int[] findNumbersLessN(int number) {
        int quantity = (int) Math.sqrt(number);
        int[] numbers = new int[quantity];
        for (int i = 0; i < quantity; i++) {
            numbers[i] = (int) Math.pow(i + 1, 2);
        }
        return numbers;
    }

    public static void printArray(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            if (i == numbers.length - 1) {
                System.out.print(numbers[i]);
            } else {
                System.out.print(numbers[i] + ", ");
            }
        }
    }
}