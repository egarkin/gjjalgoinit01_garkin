package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 08.07.2016.
 */
public class TaskCh10N055 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input number");
        int number = scanner.nextInt();
        System.out.println("Input numeral system");
        int numeralSystem = scanner.nextInt();

        String result = countNumber(number, numeralSystem);
        System.out.println("Number \"" + number + "\" in " + numeralSystem
                + " numeral system is " + result);
    }

    public static String countNumber(int number, int numeralSystem) {
        char[] hexSymbols = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        if (numeralSystem == 10) {
            return String.valueOf(number);
        } else {
            if (number < numeralSystem) {
                return String.valueOf(hexSymbols[number]);
            } else {
                return countNumber(number / numeralSystem, numeralSystem)
                        + String.valueOf(hexSymbols[number % numeralSystem]);
            }
        }
    }
}