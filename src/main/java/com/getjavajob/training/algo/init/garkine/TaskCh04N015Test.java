package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh04N015.calculateAge;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh04N015Test {
    public static void main(String[] args) {
        testCalculateAgeBirthdayMonthEqualsTodayMonth();
        testCalculateAgeBirthdayMonthLessThanTodayMonth();
        testCalculateAgeBirthdayMonthMoreThanTodayMonth();
    }

    public static void testCalculateAgeBirthdayMonthLessThanTodayMonth() {
        assertEquals("TaskCh04N015Test.testCalculateAgeBirthdayMonthLessThanTodayMonth",
                29, calculateAge(6, 1985, 12, 2014));
    }

    public static void testCalculateAgeBirthdayMonthMoreThanTodayMonth() {
        assertEquals("TaskCh04N015Test.testCalculateAgeBirthdayMonthMoreThanTodayMonth",
                28, calculateAge(6, 1985, 5, 2014));
    }

    public static void testCalculateAgeBirthdayMonthEqualsTodayMonth() {
        assertEquals("TaskCh04N015Test.testCalculateAgeBirthdayMonthEqualsTodayMonth",
                29, calculateAge(6, 1985, 6, 2014));
    }
}
