package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh02N031.findNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh02N031Test {
    public static void main(String[] args) {
        testFindNumber();
    }

    public static void testFindNumber() {
        assertEquals("TaskCh02N031Test.testFindNumber", 247, findNumber(274));
    }
}