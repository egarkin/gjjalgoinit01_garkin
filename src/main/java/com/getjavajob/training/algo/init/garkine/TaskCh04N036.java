package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 04.07.2016.
 */
public class TaskCh04N036 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input minutes");
        int minutes = scanner.nextByte();

        String result = findColor(minutes);
        System.out.println("Color is " + result);
    }

    public static String findColor(int minutes) {
        return minutes % 5 < 3 ? "Green" : "Red";
    }
}