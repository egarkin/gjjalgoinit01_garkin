package com.getjavajob.training.algo.init.garkine;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Evgeny on 10.07.2016.
 */
public class TaskCh12N025 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input rows number");
        int rows = scanner.nextInt();
        System.out.println("Input columns number");
        int columns = scanner.nextInt();

        int[][] resultA = taskA(new int[rows][columns], rows, columns);
        int[][] resultB = taskB(new int[rows][columns], rows, columns);
        int[][] resultC = taskC(new int[rows][columns], rows, columns);
        int[][] resultD = taskD(new int[rows][columns], rows, columns);
        int[][] resultE = taskE(new int[rows][columns], rows, columns);
        int[][] resultF = taskF(new int[rows][columns], rows, columns);
        int[][] resultG = taskG(new int[rows][columns], rows, columns);
        int[][] resultH = taskH(new int[rows][columns], rows, columns);
        int[][] resultI = taskI(new int[rows][columns], rows, columns);
        int[][] resultJ = taskJ(new int[rows][columns], rows, columns);
        int[][] resultK = taskK(new int[rows][columns], rows, columns);
        int[][] resultL = taskL(new int[rows][columns], rows, columns);
        int[][] resultM = taskM(new int[rows][columns], rows, columns);
        int[][] resultN = taskN(new int[rows][columns], rows, columns);
        int[][] resultO = taskO(new int[rows][columns], rows, columns);
        int[][] resultP = taskP(new int[rows][columns], rows, columns);
        System.out.println("Task A:\n" + Arrays.deepToString(resultA));
        System.out.println("\nTask B:\n" + Arrays.deepToString(resultB));
        System.out.println("\nTask C:\n" + Arrays.deepToString(resultC));
        System.out.println("\nTask D:\n" + Arrays.deepToString(resultD));
        System.out.println("\nTask E:\n" + Arrays.deepToString(resultE));
        System.out.println("\nTask F:\n" + Arrays.deepToString(resultF));
        System.out.println("\nTask G:\n" + Arrays.deepToString(resultG));
        System.out.println("\nTask H:\n" + Arrays.deepToString(resultH));
        System.out.println("\nTask I:\n" + Arrays.deepToString(resultI));
        System.out.println("\nTask J:\n" + Arrays.deepToString(resultJ));
        System.out.println("\nTask K:\n" + Arrays.deepToString(resultK));
        System.out.println("\nTask L:\n" + Arrays.deepToString(resultL));
        System.out.println("\nTask M:\n" + Arrays.deepToString(resultM));
        System.out.println("\nTask N:\n" + Arrays.deepToString(resultN));
        System.out.println("\nTask O:\n" + Arrays.deepToString(resultO));
        System.out.println("\nTask P:\n" + Arrays.deepToString(resultP));
    }

    public static int[][] taskA(int[][] array, int rows, int columns) {
        int value = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                array[i][j] = ++value;
            }
        }
        return array;
    }

    public static int[][] taskB(int[][] array, int rows, int columns) {
        int value = 0;
        for (int j = 0; j < columns; j++) {
            for (int i = 0; i < rows; i++) {
                array[i][j] = ++value;
            }
        }
        return array;
    }

    public static int[][] taskC(int[][] array, int rows, int columns) {
        int value = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = columns - 1; j >= 0; j--) {
                array[i][j] = ++value;
            }
        }
        return array;
    }

    public static int[][] taskD(int[][] array, int rows, int columns) {
        int value = 0;
        for (int j = 0; j < columns; j++) {
            for (int i = rows - 1; i >= 0; i--) {
                array[i][j] = ++value;
            }
        }
        return array;
    }

    private static int[][] taskE(int[][] array, int rows, int columns) {
        int value = 0;
        for (int i = 0; i < rows; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < columns; j++) {
                    array[i][j] = ++value;
                }
            } else {
                for (int j = columns - 1; j >= 0; j--) {
                    array[i][j] = ++value;
                }
            }
        }
        return array;
    }

    private static int[][] taskF(int[][] array, int rows, int columns) {
        int value = 0;
        for (int j = 0; j < columns; j++) {
            if (j % 2 == 0) {
                for (int i = 0; i < rows; i++) {
                    array[i][j] = ++value;
                }
            } else {
                for (int i = rows - 1; i >= 0; i--) {
                    array[i][j] = ++value;
                }
            }
        }
        return array;
    }

    public static int[][] taskG(int[][] array, int rows, int columns) {
        int value = 0;
        for (int i = rows - 1; i >= 0; i--) {
            for (int j = 0; j < columns; j++) {
                array[i][j] = ++value;
            }
        }
        return array;
    }

    public static int[][] taskH(int[][] array, int rows, int columns) {
        int value = 0;
        for (int j = columns - 1; j >= 0; j--) {
            for (int i = 0; i < rows; i++) {
                array[i][j] = ++value;
            }
        }
        return array;
    }

    public static int[][] taskI(int[][] array, int rows, int columns) {
        int value = 0;
        for (int i = rows - 1; i >= 0; i--) {
            for (int j = columns - 1; j >= 0; j--) {
                array[i][j] = ++value;
            }
        }
        return array;
    }

    public static int[][] taskJ(int[][] array, int rows, int columns) {
        int value = 0;
        for (int j = columns - 1; j >= 0; j--) {
            for (int i = rows - 1; i >= 0; i--) {
                array[i][j] = ++value;
            }
        }
        return array;
    }

    public static int[][] taskK(int[][] array, int rows, int columns) {
        int value = 0;
        for (int i = rows - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = columns - 1; j >= 0; j--) {
                    array[i][j] = ++value;
                }
            } else {
                for (int j = 0; j < columns; j++) {
                    array[i][j] = ++value;
                }
            }
        }
        return array;
    }

    public static int[][] taskL(int[][] array, int rows, int columns) {
        int value = 0;
        for (int i = 0; i < rows; i++) {
            if (i % 2 == 0) {
                for (int j = columns - 1; j >= 0; j--) {
                    array[i][j] = ++value;
                }
            } else {
                for (int j = 0; j < columns; j++) {
                    array[i][j] = ++value;
                }
            }
        }
        return array;
    }

    public static int[][] taskM(int[][] array, int rows, int columns) {
        int value = 0;
        for (int j = columns - 1; j >= 0; j--) {
            if (j % 2 == 0) {
                for (int i = rows - 1; i >= 0; i--) {
                    array[i][j] = ++value;
                }
            } else {
                for (int i = 0; i < rows; i++) {
                    array[i][j] = ++value;
                }
            }
        }
        return array;
    }

    public static int[][] taskN(int[][] array, int rows, int columns) {
        int value = 0;
        for (int j = 0; j < columns; j++) {
            if (j % 2 == 0) {
                for (int i = rows - 1; i >= 0; i--) {
                    array[i][j] = ++value;
                }
            } else {
                for (int i = 0; i < rows; i++) {
                    array[i][j] = ++value;
                }
            }
        }
        return array;
    }

    public static int[][] taskO(int[][] array, int rows, int columns) {
        int value = 0;
        for (int i = rows - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = 0; j < columns; j++) {
                    array[i][j] = ++value;
                }
            } else {
                for (int j = columns - 1; j >= 0; j--) {
                    array[i][j] = ++value;
                }
            }
        }
        return array;
    }

    public static int[][] taskP(int[][] array, int rows, int columns) {
        int value = 0;
        for (int j = columns - 1; j >= 0; j--) {
            if (j % 2 == 0) {
                for (int i = 0; i < rows; i++) {
                    array[i][j] = ++value;
                }
            } else {
                for (int i = rows - 1; i >= 0; i--) {
                    array[i][j] = ++value;
                }
            }
        }
        return array;
    }
}
