package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N053.reverseArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh10N053Test {
    public static void main(String[] args) {
        testReverseArrayThanLengthEven();
        testReverseArrayThanLengthOdd();
    }

    public static void testReverseArrayThanLengthEven() {
        int[] expected = {22, 54, 23, 102, 37, 20, 15, 10};
        int[] actual = {10, 15, 20, 37, 102, 23, 54, 22};
        assertEquals("TaskCh10N053Test.testReverseArrayThanLengthEven", expected, reverseArray(actual, 0));
    }

    public static void testReverseArrayThanLengthOdd() {
        int[] expected = {22, 54, 23, 102, 98, 37, 20, 15, 10};
        int[] actual = {10, 15, 20, 37, 98, 102, 23, 54, 22};
        assertEquals("TaskCh10N053Test.testReverseArrayThanLengthOdd", expected, reverseArray(actual, 0));
    }
}
