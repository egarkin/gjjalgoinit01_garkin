package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 07.07.2016.
 */
public class TaskCh10N045 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input first term");
        int firstTerm = scanner.nextInt();
        System.out.println("Input difference");
        int difference = scanner.nextInt();
        System.out.println("Input number to calculate term");
        int number = scanner.nextInt();

        int term = calculateTerm(firstTerm, difference, number);
        int sumOfTerms = calculateSumOfTerms(firstTerm, difference, number);
        System.out.println("The " + number + " term is " + term);
        System.out.println("The sum of " + number + " terms is " + sumOfTerms);
    }

    public static int calculateTerm(int firstTerm, int difference, int number) {
        if (number == 1) {
            return firstTerm;
        } else {
            return calculateTerm(firstTerm, difference, number - 1) + difference;
        }
    }

    public static int calculateSumOfTerms(int firstTerm, int difference, int number) {
        if (number == 1) {
            return firstTerm;
        } else {
            return calculateTerm(firstTerm, difference, number)
                    + calculateSumOfTerms(firstTerm, difference, number - 1);
        }
    }
}