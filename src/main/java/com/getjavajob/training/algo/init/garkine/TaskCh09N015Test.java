package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh09N015.findSymbol;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh09N015Test {
    public static void main(String[] args) {
        testFindSymbol();
    }

    public static void testFindSymbol() {
        assertEquals("TaskCh09N015Test.testFindSymbol", 'e', findSymbol("Hello", 2));
    }
}
