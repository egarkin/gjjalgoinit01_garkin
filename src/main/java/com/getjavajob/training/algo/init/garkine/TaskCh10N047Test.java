package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N047.fibonacci;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh10N047Test {
    public static void main(String[] args) {
        testFibonacci();
    }

    public static void testFibonacci() {
        assertEquals("TaskCh10N047Test.testFibonacci", 8, fibonacci(6));
    }
}
