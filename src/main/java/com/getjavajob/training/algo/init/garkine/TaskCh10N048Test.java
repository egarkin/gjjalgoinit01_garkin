package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N048.findMax;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh10N048Test {
    public static void main(String[] args) {
        testFindMax();
    }

    public static void testFindMax() {
        int[] actual = {1, 5, 18, 3, 2};
        assertEquals("TaskCh10N048Test.testFindMax", 18, findMax(actual, 5));
    }
}
