package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Evgeny on 10.07.2016.
 */
public class TaskCh12N063 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int rows = 4;
        int columns = 11;
        int[][] classes = new int[columns][rows];
        char[] classChar = new char[]{'a', 'b', 'c', 'd'};

        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                System.out.println("Input number of students in " + (i + 1) + classChar[j] + " class");
                classes[i][j] = scanner.nextInt();
            }
        }

        printArray(classes, columns);
    }

    public static int calculateAverageNumber(int[] group) {
        int sum = 0;
        for (int i : group) {
            sum += i;
        }
        return sum / 4;
    }

    public static void printArray(int[][] array, int columns) {
        for (int i = 0; i < columns; i++) {
            System.out.println("the average number of students in " + (i + 1) + " classes is "
                    +  calculateAverageNumber(array[i]));
        }
    }
}