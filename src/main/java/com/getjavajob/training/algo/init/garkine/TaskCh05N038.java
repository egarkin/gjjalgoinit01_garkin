package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 04.07.2016.
 */
public class TaskCh05N038 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input N");
        int n = scanner.nextInt();

        double distance = calculateDistance(n);
        double path = calculatePath(n);
        System.out.println("Man is " + distance + " kilometres from home and held " + path + " kilometres");
    }

    public static double calculateDistance(int n) {
        double result = 0;
        for (int i = 1; i <= n; i += 2) {
            result += (double) 1 / i;
        }
        for (int i = 2; i <= n; i += 2) {
            result += (double) -1 / i;
        }
        return result;
    }

    public static double calculatePath(int n) {
        double result = 1;
        for (int i = 2; i <= n; i++) {
            result += (double) 1 / i;
        }
        return result;
    }
}