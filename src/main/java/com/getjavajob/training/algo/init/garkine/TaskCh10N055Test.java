package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N055.countNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh10N055Test {
    public static void main(String[] args) {
        testCountNumberDecToHex();
        testCountNumberDecToBin();
    }

    public static void testCountNumberDecToHex() {
        assertEquals("TaskCh10N055Test.testCountNumberDecToHex", "FE", countNumber(254, 16));
    }

    public static void testCountNumberDecToBin() {
        assertEquals("TaskCh10N055Test.testCountNumberDecToBin", "1101", countNumber(13, 2));
    }
}