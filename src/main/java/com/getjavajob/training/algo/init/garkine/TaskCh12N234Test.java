package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh12N234.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh12N234Test {
    public static void main(String[] args) {
        testDeleteRow();
        testDeleteColumn();
    }

    public static void testDeleteRow() {
        int[][] expected = {
                {1, 2, 3, 5, 0},
                {1, 2, 3, 5, 0},
                {1, 2, 3, 5, 0},
                {1, 2, 3, 5, 0},
                {1, 2, 3, 5, 0}
        };
        int[][] actual = {
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5}
        };
        assertEquals("TaskCh12N234Test.testDeleteRow", expected, deleteRow(actual, 3));
    }

    public static void testDeleteColumn() {
        int[][] expected = {
                {1, 1, 1, 1, 1},
                {2, 2, 2, 2, 2},
                {4, 4, 4, 4, 4},
                {5, 5, 5, 5, 5},
                {0, 0, 0, 0, 0}
        };
        int[][] actual = {
                {1, 1, 1, 1, 1},
                {2, 2, 2, 2, 2},
                {3, 3, 3, 3, 3},
                {4, 4, 4, 4, 4},
                {5, 5, 5, 5, 5}
        };
        assertEquals("TaskCh12N234Test.testDeleteColumn", expected, deleteColumn(actual, 2));
    }
}