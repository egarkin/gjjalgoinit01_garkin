package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N045.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh10N045Test {
    public static void main(String[] args) {
        testCalculateTerm();
        testCalculateSumOfTerms();
    }

    public static void testCalculateTerm() {
        assertEquals("TaskCh10N045Test.testCalculateTerm", 5, calculateTerm(3, 1, 3));
    }

    public static void testCalculateSumOfTerms() {
        assertEquals("TaskCh10N045Test.testCalculateSumOfTerms", 9, calculateSumOfTerms(2, 1, 3));
    }
}
