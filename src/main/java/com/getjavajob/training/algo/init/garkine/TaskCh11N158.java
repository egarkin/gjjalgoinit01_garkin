package com.getjavajob.training.algo.init.garkine;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Evgeny on 10.07.2016.
 */
public class TaskCh11N158 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input array length");
        int length = scanner.nextInt();
        int[] array = new int[length];
        for (int i = 0; i < length; i++) {
            System.out.println("Input " + (i + 1) + " element");
            array[i] = scanner.nextInt();
        }

        array = removeDuplicateElements(array);
        System.out.println(Arrays.toString(array));
    }

    public static int[] removeDuplicateElements(int[] array) {
        int[] temp = new int[array.length];
        int numberOfNonDuplicateElements = 1;
        temp[0] = array[0];
        for (int i = 1; i < array.length; i++) {
            boolean isDuplicate = false;
            for (int j = 0; j < numberOfNonDuplicateElements && !isDuplicate; j++) {
                if (array[i] == temp[j]) {
                    isDuplicate = true;
                }
            }
            if (!isDuplicate) {
                temp[numberOfNonDuplicateElements] = array[i];
                numberOfNonDuplicateElements++;
            }
        }
        return temp;
    }
}