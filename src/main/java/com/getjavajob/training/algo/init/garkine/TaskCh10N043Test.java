package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N043.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh10N043Test {
    public static void main(String[] args) {
        testSumDigits();
        testAmountDigits();
    }

    public static void testSumDigits() {
        assertEquals("TaskCh10N043Test.testSumDigits", 6, sumDigits(132));
    }

    public static void testAmountDigits() {
        assertEquals("TaskCh10N043Test.testAmountDigits", 4, amountDigits(1547));
    }
}