package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 03.07.2016.
 */
public class TaskCh01N003 {
    public static void main(String[] args) {
        boolean isNumber = false;

        while (!isNumber) {
            System.out.println("Enter the number");
            Scanner scanner = new Scanner(System.in);
            if (scanner.hasNextInt()) {
                isNumber = true;
                int number = scanner.nextInt();
                System.out.println("You have entered number " + number);
            } else {
                System.out.println("You have entered not a number! Retype please.\n");
            }
        }
    }
}
