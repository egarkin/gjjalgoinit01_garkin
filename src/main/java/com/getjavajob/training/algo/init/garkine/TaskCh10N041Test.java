package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N041.factorial;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh10N041Test {
    public static void main(String[] args) {
        testFactorial();
    }

    public static void testFactorial() {
        assertEquals("TaskCh10N041Test.testFactorial", 24, factorial(4));
    }
}
