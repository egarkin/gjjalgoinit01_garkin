package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 04.07.2016.
 */
public class TaskCh05N064 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] districts = new int[12][2];
        for (int i = 0; i < districts.length; i++) {
            System.out.println("Input number of people for " + (i + 1) + " district");
            districts[i][0] = scanner.nextInt();
            System.out.println("Input area for " + (i + 1) + " district");
            districts[i][1] = scanner.nextInt();
        }

        double result = calculateAveragePopulation(districts);
        System.out.println("Average population is " + result);
    }

    public static double calculateAveragePopulation(int[][] districts) {
        double average = 0;
        for (int[] district : districts) {
            average += (double) district[0] / district[1];
        }
        return average;
    }
}