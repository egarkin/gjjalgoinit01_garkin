package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh09N107.swapLetters;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh09N107Test {
    public static void main(String[] args) {
        testSwapLetters();
        testNoLettersToSwap();
    }

    public static void testSwapLetters() {
        assertEquals("TaskCh09N107Test.testSwapLetters", "kjohagogfpa",
                swapLetters(new StringBuilder("kjahagogfpo")));
    }

    public static void testNoLettersToSwap() {
        assertEquals("TaskCh09N107Test.testNoLettersToSwap", "-1",
                swapLetters(new StringBuilder("kjegog")));
    }
}
