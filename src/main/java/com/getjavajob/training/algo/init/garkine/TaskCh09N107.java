package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 06.07.2016.
 */
public class TaskCh09N107 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input word");
        StringBuilder builder = new StringBuilder(scanner.nextLine().toLowerCase());

        String result = swapLetters(builder).toString();
        if (result.equals("-1")) {
            System.out.println("Letters \"a\" and \"o\" can`t be swapped");
        } else {
            System.out.println("Word after swap - \"" + builder + "\"");
        }
    }

    public static StringBuilder swapLetters(StringBuilder word) {
        int aFirstIndex = word.indexOf("a");
        int oLastIndex = word.lastIndexOf("o");
        if (aFirstIndex == -1 || oLastIndex == -1) {
            return new StringBuilder("-1");
        } else {
            word.setCharAt(aFirstIndex, 'o');
            word.setCharAt(oLastIndex, 'a');
        }
        return word;
    }
}