package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh12N063.calculateAverageNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh12N063Test {
    public static void main(String[] args) {
        testCalculateAverageNumber();
    }

    public static void testCalculateAverageNumber() {
        int[] actual = {10, 12, 14, 16};
        assertEquals("TaskCh12N063Test.testCalculateAverageNumber", 13, calculateAverageNumber(actual));
    }
}
