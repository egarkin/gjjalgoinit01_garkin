package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 06.07.2016.
 */
public class TaskCh09N042 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input word");
        String word = scanner.nextLine();

        StringBuilder result = reverseWord(word);
        System.out.println("Word beginning with the last letter is \"" + result + "\"");
    }

    public static StringBuilder reverseWord(String word) {
        StringBuilder builder = new StringBuilder(word);
        return builder.reverse();
    }
}