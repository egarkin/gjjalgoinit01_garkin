package com.getjavajob.training.algo.init.garkine;

import static java.lang.Math.*;

/**
 * Created by Johnny on 03.07.2016.
 */
public class TaskCh01N017 {
    public static void main(String[] args) {
        double resultO = taskO(0);
        double resultP = taskP(1, 1, 4, 3);
        double resultR = taskR(2);
        double resultS = taskS(2);

        System.out.println("Task 0 " + resultO);
        System.out.println("Task P " + resultP);
        System.out.println("Task R " + resultR);
        System.out.println("Task S " + resultS);
    }

    public static double taskO(double x) {
        return sqrt(1 - pow(sin(x), 2));
    }

    public static double taskP(double a, double b, double c, double x) {
        return 1 / sqrt(a * pow(x, 2) + b * x + c);
    }

    public static double taskR(double x) {
        return (sqrt(x + 1) + sqrt(x - 1)) / 2 * sqrt(x);
    }

    public static double taskS(double x) {
        return abs(x) + abs(x + 1);
    }
}
