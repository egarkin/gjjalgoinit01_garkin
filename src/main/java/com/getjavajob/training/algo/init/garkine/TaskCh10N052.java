package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N043.amountDigits;

/**
 * Created by Johnny on 08.07.2016.
 */
public class TaskCh10N052 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input number");
        int number = scanner.nextInt();

        int result = reverseDigits(number);
        System.out.print("Number \"" + number + "\" digits in reverse order is " + result);
    }

    public static int reverseDigits(int number) {
        if (number >= 0 && number <= 9) {
            return number;
        } else {
            int temp = number % 10 * (int) Math.pow(10, amountDigits(number) - 1);
            return temp + reverseDigits(number / 10);
        }
    }
}