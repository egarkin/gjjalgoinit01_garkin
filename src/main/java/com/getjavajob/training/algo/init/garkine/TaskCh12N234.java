package com.getjavajob.training.algo.init.garkine;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Evgeny on 10.07.2016.
 */
public class TaskCh12N234 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input array length");
        int length = scanner.nextInt();
        System.out.println("Input row number");
        int row = scanner.nextInt();
        System.out.println("Input column number");
        int column = scanner.nextInt();
        int[][] arrayA = new int[length][length];
        int[][] arrayB = new int[length][length];
        for (int i = 0; i < arrayA.length; i++) {
            for (int j = 0; j < arrayA.length; j++) {
                arrayA[i][j] = j + 1;
                arrayB[i][j] = i + 1;
            }
        }

        int[][] deleteRow = deleteRow(arrayA, row);
        int[][] deleteColumn = deleteColumn(arrayB, column);
        System.out.println("Task A:\n" + Arrays.deepToString(deleteRow));
        System.out.println("\nTask B:\n" + Arrays.deepToString(deleteColumn));
    }

    public static int[][] deleteRow(int[][] array, int row) {
        int[][] temp = new int[array.length][array.length];

        for (int j = 0; j < row; j++) {
            for (int i = 0; i < array.length; i++) {
                temp[i][j] = array[i][j];
            }
        }

        for (int j = row + 1; j < array.length; j++) {
            for (int i = 0; i < array.length; i++) {
                temp[i][j - 1] = array[i][j];
            }
        }
        return temp;
    }

    public static int[][] deleteColumn(int[][] array, int column) {
        int[][] temp = new int[array.length][array.length];

        for (int i = 0; i < column; i++) {
            System.arraycopy(array[i], 0, temp[i], 0, array.length);
        }

        for (int i = column + 1; i < array.length; i++) {
            System.arraycopy(array[i], 0, temp[i - 1], 0, array.length);
        }
        return temp;
    }
}