package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh02N043.isDivisible;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh02N043Test {
    public static void main(String[] args) {
        testIsDivisible();
        testDivisionByZero();
        testIsNotDivisible();
    }

    public static void testIsDivisible() {
        assertEquals("TaskCh02N043Test.testIsDivisible", 1, isDivisible(2, 2));
    }

    public static void testDivisionByZero() {
        assertEquals("TaskCh02N043Test.testDivisionByZero", 0, isDivisible(0, 2));
    }

    public static void testIsNotDivisible() {
        assertEquals("TaskCh02N043Test.testIsNotDivisible", 3, isDivisible(4, 2));
    }
}
