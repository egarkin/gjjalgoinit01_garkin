package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh02N039.calculateAngle;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh02N039Test {
    public static void main(String[] args) {
        testCalculateAngle();
    }

    public static void testCalculateAngle() {
        assertEquals("TaskCh02N039Test.testCalculateAngle", 180, calculateAngle(12, 0, 0));
    }
}
