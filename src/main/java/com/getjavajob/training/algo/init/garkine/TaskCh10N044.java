package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N043.sumDigits;

/**
 * Created by Johnny on 07.07.2016.
 */
public class TaskCh10N044 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input number");
        int number = scanner.nextInt();

        int result = digitalSqrt(number);
        System.out.println("Digital sqrt of number \"" + number + "\" is " + result);
    }

    public static int digitalSqrt(int number) {
        if (number >= 0 && number <= 9) {
            return number;
        } else {
            return digitalSqrt(sumDigits(number));
        }
    }
}