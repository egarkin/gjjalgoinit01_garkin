package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh05N010.calculateRubles;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh05N010Test {
    public static void main(String[] args) {
        testCalculateRubles();
    }

    public static void testCalculateRubles() {
        double[] expectedTable = {30.0, 60.0, 90.0, 120.0, 150.0, 180.0, 210.0, 240.0, 270.0, 300.0, 330.0,
                360.0, 390.0, 420.0, 450.0, 480.0, 510.0, 540.0, 570.0, 600.0};
        assertEquals("TaskCh03N026Test.testTaskA", expectedTable, calculateRubles(30.0));
    }
}
