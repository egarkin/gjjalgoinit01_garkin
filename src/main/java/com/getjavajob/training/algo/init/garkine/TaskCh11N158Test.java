package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh11N158.removeDuplicateElements;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh11N158Test {
    public static void main(String[] args) {
        testRemoveDuplicateElements();
    }

    public static void testRemoveDuplicateElements() {
        int[] expected = {14, 10, 12, 18, 17, 0, 0};
        int[] actual = {14, 10, 12, 14, 18, 14, 17};
        assertEquals("TaskCh11N158Test.testRemoveDuplicateElements", expected, removeDuplicateElements(actual));
    }
}