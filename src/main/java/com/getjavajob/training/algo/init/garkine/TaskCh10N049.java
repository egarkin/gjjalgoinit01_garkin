package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 07.07.2016.
 */
public class TaskCh10N049 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input array size");
        int size = scanner.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            System.out.println("Input " + (i + 1) + " element");
            array[i] = scanner.nextInt();
        }

        int result = findIndexOfMaxElement(array, size);
        System.out.println("Index of maximum element is " + result);
    }

    public static int findIndexOfMaxElement(int[] array, int size) {
        if (size == 0) {
            return 0;
        } else {
            int index = findIndexOfMaxElement(array, size - 1);
            return array[size - 1] > array[index] ? (size - 1) : index;
        }
    }
}