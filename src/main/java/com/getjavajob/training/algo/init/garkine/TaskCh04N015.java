package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 04.07.2016.
 */
public class TaskCh04N015 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter current month");
        int monthNow = scanner.nextInt();
        System.out.println("Enter current year");
        int yearNow = scanner.nextInt();
        System.out.println("Enter the number month of birth");
        int month = scanner.nextInt();
        System.out.println("Enter year of birth");
        int year = scanner.nextInt();

        int result = calculateAge(month, year, monthNow, yearNow);
        System.out.println("Person is " + result + " years old");
    }

    public static int calculateAge(int month, int year, int monthNow, int yearNow) {
        int age = yearNow - year;
        return month <= monthNow ? age : --age;
    }
}