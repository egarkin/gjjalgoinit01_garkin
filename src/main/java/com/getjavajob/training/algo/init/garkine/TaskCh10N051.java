package com.getjavajob.training.algo.init.garkine;


/**
 * Created by Johnny on 07.07.2016.
 */
public class TaskCh10N051 {
    public static void main(String[] args) {
        int n = 5;

        System.out.println("Task A:");
        taskA(n);
        System.out.println("\nTask B:");
        taskB(n);
        System.out.println("\nTask C:");
        taskC(n);
    }

    public static void taskA(int n) {
        if (n > 0) {
            System.out.println("n = " + n);
            taskA(n - 1);
        }
    }

    public static void taskB(int n) {
        if (n > 0) {
            taskB(n - 1);
            System.out.println("n = " + n);
        }
    }

    public static void taskC(int n) {
        if (n > 0) {
            System.out.println("n = " + n);
            taskC(n - 1);
            System.out.println("n = " + n);
        }
    }
}