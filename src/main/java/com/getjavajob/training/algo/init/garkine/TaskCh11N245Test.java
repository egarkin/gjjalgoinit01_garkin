package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh11N245.sortArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh11N245Test {
    public static void main(String[] args) {
        testSortArray();
    }

    public static void testSortArray() {
        int[] expected = {-10, -12, -18, 14, 19, 11, 17};
        int[] actual = {14, -10, -12, 19, -18, 11, 17};
        assertEquals("TaskCh11N245Test.testSortArray", expected, sortArray(actual));
    }
}