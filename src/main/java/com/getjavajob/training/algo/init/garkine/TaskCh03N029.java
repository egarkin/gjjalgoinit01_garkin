package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 04.07.2016.
 */
public class TaskCh03N029 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input X");
        int x = scanner.nextInt();
        System.out.println("Input Y");
        int y = scanner.nextInt();
        System.out.println("Input Z");
        int z = scanner.nextInt();

        boolean resultA = taskA(x, y);
        boolean resultB = taskB(x, y);
        boolean resultC = taskC(x, y);
        boolean resultD = taskD(x, y, z);
        boolean resultE = taskE(x, y, z);
        boolean resultF = taskF(x, y, z);

        System.out.println("Task A result " + resultA);
        System.out.println("Task B result " + resultB);
        System.out.println("Task C result " + resultC);
        System.out.println("Task D result " + resultD);
        System.out.println("Task E result " + resultE);
        System.out.println("Task F result " + resultF);
    }

    /**
     * Returns true, than each of the numbers X and Y is odd
     *
     * @param x first number, that input from console
     * @param y second number, that input from console
     * @return statement's boolean value
     */
    public static boolean taskA(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    /**
     * Returns true, than only one of the numbers X and Y is less than 20
     *
     * @param x first number, that input from console
     * @param y second number, that input from console
     * @return statement's boolean value
     */
    public static boolean taskB(int x, int y) {
        return x < 20 ^ y < 20;
    }

    /**
     * Returns true, than at least one of the numbers X and Y equals zero
     *
     * @param x first number, that input from console
     * @param y second number, that input from console
     * @return statement's boolean value
     */
    public static boolean taskC(int x, int y) {
        return x == 0 || y == 0;
    }

    /**
     * Returns true, than each of the numbers X, Y, Z is negative
     *
     * @param x first number, that input from console
     * @param y second number, that input from console
     * @param z third number, that input from console
     * @return statement's boolean value
     */
    public static boolean taskD(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    /**
     * Returns true, than only one of the numbers X, Y, Z multiple five
     *
     * @param x first number, that input from console
     * @param y second number, that input from console
     * @param z third number, that input from console
     * @return statement's boolean value
     */
    public static boolean taskE(int x, int y, int z) {
        boolean isFirstNumberMultipleFive = x % 5 == 0;
        boolean isSecondNumberMultipleFive = y % 5 == 0;
        boolean isThirdNumberMultipleFive = z % 5 == 0;

        return isFirstNumberMultipleFive && !isSecondNumberMultipleFive && !isThirdNumberMultipleFive
                || !isFirstNumberMultipleFive && isSecondNumberMultipleFive && !isThirdNumberMultipleFive
                || !isFirstNumberMultipleFive && !isSecondNumberMultipleFive && isThirdNumberMultipleFive;
    }

    /**
     * Returns true, than at least one of the numbers X, Y, Z is more than 100
     *
     * @param x first number, that input from console
     * @param y second number, that input from console
     * @param z third number, that input from console
     */
    public static boolean taskF(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}