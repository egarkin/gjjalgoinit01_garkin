package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh04N106.calculateSeason;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh04N106Test {
    public static void main(String[] args) {
        testSpring();
        testSummer();
        testAutumn();
        testWinter();
    }

    public static void testWinter() {
        assertEquals("TaskCh04N106Test.testWinter", "Winter", calculateSeason(1));
    }

    public static void testSpring() {
        assertEquals("TaskCh04N106Test.testSpring", "Spring", calculateSeason(4));
    }

    public static void testSummer() {
        assertEquals("TaskCh04N106Test.testSummer", "Summer", calculateSeason(6));
    }

    public static void testAutumn() {
        assertEquals("TaskCh04N106Test.testAutumn", "Spring", calculateSeason(4));
    }
}
