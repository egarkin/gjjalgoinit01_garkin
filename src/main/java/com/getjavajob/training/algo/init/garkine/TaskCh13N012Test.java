package com.getjavajob.training.algo.init.garkine;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh13N012Test {
    public static void main(String[] args) {
        testFindEmployeesWhoWorkMoreThanInputYears();
        testFindEmployeesWithInputSubstring();
    }

    public static void testFindEmployeesWhoWorkMoreThanInputYears() {
        Database employeesData = new Database();
        employeesData.addEmployee(new Employee("Garkin", "Evgeny", "Ferganskaya 9", 5, 2015));
        employeesData.addEmployee(new Employee("Ivanov", "Petr", "Ivanovich", "Tverskaya 10", 9, 2010));
        employeesData.addEmployee(new Employee("Petrov", "Alex", "Lenina 5", 7, 2013));
        List<Employee> employeesToCompare = new ArrayList();
        employeesToCompare.add(new Employee("Ivanov", "Petr", "Ivanovich", "Tverskaya 10", 9, 2010));
        employeesToCompare.add(new Employee("Petrov", "Alex", "Lenina 5", 7, 2013));
        assertEquals("TaskCh13N012Test.testFindEmployeesWhoWorkMoreThanInputYears", employeesToCompare,
                employeesData.employeesWhoWorkMoreThanInputYears(3, 7, 2016));
    }

    public static void testFindEmployeesWithInputSubstring() {
        Database employeesData = new Database();
        employeesData.addEmployee(new Employee("Garkin", "Evgeny", "Ferganskaya 9", 5, 2015));
        employeesData.addEmployee(new Employee("Ivanov", "Petr", "Ivanovich", "Tverskaya 10", 9, 2010));
        employeesData.addEmployee(new Employee("Petrov", "Alex", "Lenina 5", 7, 2013));
        List<Employee> employeesToCompare = new ArrayList();
        employeesToCompare.add(new Employee("Ivanov", "Petr", "Ivanovich", "Tverskaya 10", 9, 2010));
        assertEquals("TaskCh13N012Test.testFindEmployeesWithInputSubstring", employeesToCompare,
                employeesData.employeesWithInputSubstring("IVAN"));
    }
}
