package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 07.07.2016.
 */
public class TaskCh10N046 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input first term");
        int firstTerm = scanner.nextInt();
        System.out.println("Input denominator");
        int denominator = scanner.nextInt();
        System.out.println("Input number to calculate term");
        int number = scanner.nextInt();

        int term = calculateTerm(firstTerm, denominator, number);
        int sumOfTerms = calculateSumOfTerms(firstTerm, denominator, number);
        System.out.println("The " + number + " term is " + term);
        System.out.println("The sum of " + number + " terms is " + sumOfTerms);
    }

    public static int calculateTerm(int firstTerm, int denominator, int number) {
        if (number == 1) {
            return firstTerm;
        } else {
            return calculateTerm(firstTerm, denominator, number - 1) * denominator;
        }
    }

    public static int calculateSumOfTerms(int firstTerm, int denominator, int number) {
        if (number == 1) {
            return firstTerm;
        } else {
            return calculateTerm(firstTerm, denominator, number)
                    + calculateSumOfTerms(firstTerm, denominator, number - 1);
        }
    }
}

