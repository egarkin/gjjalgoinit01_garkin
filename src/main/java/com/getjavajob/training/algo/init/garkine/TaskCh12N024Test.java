package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh12N024.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh12N024Test {
    public static void main(String[] args) {
        testTaskA();
        testTaskB();
    }

    public static void testTaskA() {
        int[][] expected = {
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}
        };
        assertEquals("TaskCh12N024Test.testTaskA", expected, taskA(new int[6][6]));
    }

    public static void testTaskB() {
        int[][] expected = {
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5}
        };
        assertEquals("TaskCh12N024Test.testTaskB", expected, taskB(new int[6][6]));
    }
}
