package com.getjavajob.training.algo.init.garkine;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Evgeny on 10.07.2016.
 */
public class TaskCh11N245 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input array length");
        int length = scanner.nextInt();
        int[] array = new int[length];
        for (int i = 0; i < length; i++) {
            System.out.println("Input " + (i + 1) + " element");
            array[i] = scanner.nextInt();
        }

        array = sortArray(array);
        System.out.println(Arrays.toString(array));
    }

    public static int[] sortArray(int[] array) {
        int[] positiveNumbers = new int[array.length];
        int[] temp = new int[array.length];
        int numberOfNegativeElements = 0;
        int numberOfPositiveElements = 0;

        for (int element : array) {
            if (element < 0) {
                temp[numberOfNegativeElements] = element;
                numberOfNegativeElements++;
            } else {
                positiveNumbers[numberOfPositiveElements] = element;
                numberOfPositiveElements++;
            }
        }
        System.arraycopy(positiveNumbers, 0, temp, numberOfNegativeElements, numberOfPositiveElements);
        return temp;
    }
}