package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh10N044.digitalSqrt;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh10N044Test {
    public static void main(String[] args) {
        testDigitalSqrt();
    }

    public static void testDigitalSqrt() {
        assertEquals("TaskCh10N044Test.testDigitalSqrt", 3, digitalSqrt(93));
    }
}