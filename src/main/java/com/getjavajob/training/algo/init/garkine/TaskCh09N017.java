package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 06.07.2016.
 */
public class TaskCh09N017 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input word");
        String word = scanner.nextLine();

        if (isSameLetter(word)) {
            System.out.println("Word begins and ends at the same letter");
        } else {
            System.out.println("Word begins and ends at the different letters");
        }
    }

    public static boolean isSameLetter(String word) {
        char firstLetter = word.toLowerCase().charAt(0);
        char lastLetter = word.toLowerCase().charAt(word.length() - 1);
        return firstLetter == lastLetter;
    }
}