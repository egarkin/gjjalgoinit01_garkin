package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh09N022.wordFirstPart;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh09N022Test {
    public static void main(String[] args) {
        testWordFirstPart();
    }

    public static void testWordFirstPart() {
        assertEquals("TaskCh09N022Test.testWordFirstPart", "Wo", wordFirstPart("Word"));
    }
}
