package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 08.07.2016.
 */
public class TaskCh10N056 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input number");
        int number = scanner.nextInt();

        String result = primalityTest(number, 2);
        System.out.println("Number \"" + number + "\" is " + result);
    }

    public static String primalityTest(int number, int divisor) {
        if (Math.pow(divisor, 2) > number) {
            return "a prime";
        } else if (number % divisor == 0) {
            return "not a prime";
        } else {
            return primalityTest(number, ++divisor);
        }
    }
}