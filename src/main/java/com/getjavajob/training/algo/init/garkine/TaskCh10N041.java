package com.getjavajob.training.algo.init.garkine;

import java.util.Scanner;

/**
 * Created by Johnny on 07.07.2016.
 */
public class TaskCh10N041 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input number");
        long number = scanner.nextLong();

        long result = factorial(number);
        System.out.println("Factorial of " + number + " is " + result);
    }

    public static long factorial(long number) {
        if (number == 1) {
            return 1;
        } else {
            return number * factorial(number - 1);
        }
    }
}