package com.getjavajob.training.algo.init.garkine;

import java.util.Arrays;

/**
 * Created by Johnny on 08.07.2016.
 */
public class TaskCh10N053 {
    public static void main(String[] args) {
        int[] inputArray = {10, 15, 20, 37, 98, 102, 23, 54, 22};

        int[] result = reverseArray(inputArray, 0);
        System.out.println(Arrays.toString(result));
    }

    public static int[] reverseArray(int[] inputArray, int iterator) {
        if (iterator + 1 == inputArray.length / 2) {
            int temp = inputArray[iterator];
            inputArray[iterator] = inputArray[inputArray.length - 1 - iterator];
            inputArray[inputArray.length - 1 - iterator] = temp;
            return inputArray;
        } else {
            int temp = inputArray[iterator];
            inputArray[iterator] = inputArray[inputArray.length - 1 - iterator];
            inputArray[inputArray.length - 1 - iterator] = temp;
            reverseArray(inputArray, ++iterator);
            return inputArray;
        }
    }
}