package com.getjavajob.training.algo.init.garkine;

import static com.getjavajob.training.algo.init.garkine.TaskCh02N013.reverseNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Johnny on 18.07.2016.
 */
class TaskCh02N013Test {
    public static void main(String[] args) {
        testReverseNumber();
    }

    public static void testReverseNumber() {
        assertEquals("TaskCh02N013Test.testReverseNumber", 451, reverseNumber(154));
    }
}